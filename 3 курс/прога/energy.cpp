#include "vetale.h"

double Energy(double *x,double *y, double *z, double *u,double *v, double *w)
{	
	double V1, V2, V3,V4,V5,V6, V, T = 0;

	 V1 = k * pow( (VectorLenght(0,1,x,y,z) - unstretched),2)/2;
  	 V2 = k * pow( (VectorLenght(2,3,x,y,z) - unstretched),2)/2;

  	 V3 = (4.0)/pow(VectorLenght(0,2,x,y,z),12) - (4.0)/pow(VectorLenght(0,2,x,y,z),6);
  	 V4 = (4.0)/pow(VectorLenght(0,3,x,y,z),12) - (4.0)/pow(VectorLenght(0,3,x,y,z),6);
  	 V5 = (4.0)/pow(VectorLenght(1,2,x,y,z),12) - (4.0)/pow(VectorLenght(1,2,x,y,z),6);
  	 V6 = (4.0)/pow(VectorLenght(1,3,x,y,z),12) - (4.0)/pow(VectorLenght(1,3,x,y,z),6);

	 V  = V1+V2+V3+V4+V5+V6;

	 for(int i=0;i<atoms;i++)	T += (pow(u[i],2)+pow(v[i],2)+pow(w[i],2))/2 ;
	//cout << T << " " <<  V << "\n";
	return T + V;
	
}

//______________________________________________________________



double CheckEnergy(double *mistake, double step)
{	double element, sum = 0;
	int count;
	double min,max, middle;

	ifstream file("energy.txt");

	file >> element;
	max = element;
	min = element;

	for (count = 0; count < step; count++) 
	{	
		file >> element;
		sum += element;
		if (element > max) max = element;
		if (element < min) min = element;
	}
	
	middle = sum/count;

	if (middle - min > max - middle) *mistake = middle - min;
	else *mistake = max - middle;

	file.close();
	return middle;
}


//______________________________________________________________

double CentreMassEnergy(double *u,double *v, double *w,double *uc,double *vc,double *wc)
{	double T = 0;
	MiddleVelocity(u,v,w,uc,vc,wc);

	for (int i=0;i<molecul;i++) {T += (pow(uc[i],2)+pow(vc[i],2)+pow(wc[i],2))/2; }



	return T;

}


//______________________________________________________________

double PotentialEnergy(double *x, double *y, double *z)
{	double V3,V4,V5,V6;
  	 V3 = 4/pow(VectorLenght(0,2,x,y,z),12) - 4/pow(VectorLenght(0,2,x,y,z),6);
  	 V4 = 4/pow(VectorLenght(0,3,x,y,z),12) - 4/pow(VectorLenght(0,3,x,y,z),6);
  	 V5 = 4/pow(VectorLenght(1,2,x,y,z),12) - 4/pow(VectorLenght(1,2,x,y,z),6);
  	 V6 = 4/pow(VectorLenght(1,3,x,y,z),12) - 4/pow(VectorLenght(1,3,x,y,z),6);
  	 return V3 + V4 + V5 + V6;
}


void Energ1_Out(int i, double *x,double *y, double *z, double *u,double *v, double *w)
{
ofstream file1("en_beg1.txt", ios_base::app);	
ofstream file2("en_beg2.txt", ios_base::app);	

if (i==0)file1 << Energy(x,y,z,u,v,w) <<" \n";
if (i==1)file2 << Energy(x,y,z,u,v,w) <<" \n";
file1.close();
file2.close();
}

double Energ1_Check(int i)
{	double sum = 0;
	double bet;
	ifstream file1("en_beg1.txt");	
	ifstream file2("en_beg2.txt");
	if (i==0) for (int i=0;i<rang;i++) {file1 >> bet; sum += bet;}
	if (i==1) for (int i=0;i<rang;i++) {file2 >> bet; sum += bet;}
	file1.close();
	file2.close();
	return sum;
}