#include "vetale.h"



double VectorLenght(int i, int j, double *x, double *y, double *z)
{
	return sqrt(pow( (x[j] - x[i]) ,2) + pow( (y[j] - y[i]) ,2) + pow( (z[j] - z[i]) ,2));
}

//______________________________________________________________
double RandG()
{ 
 return sqrt(-log(((double)rand())/(RAND_MAX)))/beta*sin(2.0*Pi*((double)rand())/(RAND_MAX));
}

//______________________________________________________________


void MiddleVelocity(double *u,double *v,double *w,double *uc,double *vc,double *wc)
{	for (int i=0;i<molecul;i++)
	{
		uc[i] = (u[i*2] + u[i*2+1])/2;
		vc[i] = (v[i*2] + v[i*2+1])/2;
		wc[i] = (w[i*2] + w[i*2+1])/2;
	}
}

//______________________________________________________________

void OutInFile(double *x, double *y,double *z,double *u, double *v,double *w)
{
	ofstream file1("point1.txt", ios_base::app);	
	ofstream file2("point2.txt", ios_base::app);	
	ofstream file3("point3.txt", ios_base::app);	
	ofstream file4("point4.txt", ios_base::app);

	file1 << x[0] << " " << y[0] << " " << z[0] <<" \n";
	file2 << x[1] << " " << y[1] << " " << z[1] <<" \n";
	file3 << x[2] << " " << y[2] << " " << z[2] <<" \n";
	file4 << x[3] << " " << y[3] << " " << z[3] <<" \n";

		
	file1.close();
	file2.close();
	file3.close();
	file4.close();
}
//______________________________________________________________


void OutVTK(double *x, double *y,double *z,int number)
{ 	string string;

	string = "file_" + to_string(number) + ".vtk";

	ofstream file(string);

file << "# vtk DataFile Version 2.0 \nAtoms example \nASCII \nDATASET POLYDATA \nPOINTS 4 float\n";

for (int i=0;i<atoms;i++) file << x[i] << " " << y[i] << " " << z[i] << "\n";

file << "POINT_DATA 4\nSCALARS MoleculeType float 1\nLOOKUP_TABLE default\n";

for (int i=0;i<atoms;i++) file << "1\n";
	file.close();
}


//______________________________________________________________




void OutEnergy(double *x, double *y,double *z,double *u, double *v,double *w)
{
	ofstream file5("energy.txt", ios_base::app);

	file5 << Energy(x,y,z,u,v,w) << "\n";

	file5.close();
}

//______________________________________________________________

void OutTheFinalEnergy(double step, double *x, double *y, double *z, double *u,double *v, double *w,double *uc,double *vc,double *wc)
{	double full_energy, kinetik_energy, potential_energy, mistake;
	ofstream file("final_energy.txt", ios_base::app);

	full_energy = CheckEnergy(&mistake, step);
	file << "H = "<< full_energy << " ± " << mistake;
	kinetik_energy = CentreMassEnergy(u,v,w,uc,vc,wc);
	file << "; T = " <<  kinetik_energy;
	potential_energy = PotentialEnergy(x,y,z);
	file << "; V = " << potential_energy;
	file << "; u = " << full_energy - kinetik_energy << "; ";


	file.close();
}

//______________________________________________________________

void OutTheFinal(double step, double *x, double *y, double *z, double *u,double *v, double *w,double *uc,double *vc,double *wc, double *middle, double *r)
{
	double full_energy, kinetik_energy, potential_energy, mistake;
	ofstream file(file_final, ios_base::app);

	full_energy = CheckEnergy(&mistake, step);
	file << "H = "<< full_energy << " ± " << mistake;
	kinetik_energy = CentreMassEnergy(u,v,w,uc,vc,wc);
	file << "; T = " <<  kinetik_energy;
	potential_energy = PotentialEnergy(x,y,z);
	file << "; V = " << potential_energy;
	file << "; u = " << full_energy - kinetik_energy;
	file << "; r1 = " << middle[0] << " ± " << r[0];
	file << "; r0 = " << middle[1] << " ± " << r[1] << "\n\n";


	file.close();
}


//______________________________________________________________




void OutSpecial(int i, double *x, double *y, double *z, double *u,double *v, double *w)
{
	ofstream file(file_koord, ios_base::app);
	cout << i << "\n";
	for (int i=0;i<atoms;i++) 
	{file << u[i] << " " << v[i] << " " << w[i] << " ";  }
	if (i == 1) file << "\n";
	file.close();
}



//______________________________________________________________



double kub(double *x,double *y,double *z)
{	double r[atoms];
	double sp;
	double check;
	for (int j=0;j<atoms;j++)	r[j] = sqrt(pow( (x[j]) ,2) + pow( (y[j]) ,2) + pow( (z[j]) ,2));
	for (int j=0;j<atoms;j++)
	{ 
		check = 0;
		for (int i=0;i<atoms-1;i++) 
			if (r[i]<r[i+1])
			{	sp = r[i+1];
				r[i+1] = r[i];
				r[i] = sp;
				check = 1;	
			}
		if (check == 0) break;
	}
	return r[0];
}
