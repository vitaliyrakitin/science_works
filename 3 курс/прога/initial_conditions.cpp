#include "vetale.h"

void Start()
{
	ofstream file1("point1.txt");	
	ofstream file2("point2.txt");	
	ofstream file3("point3.txt");	
	ofstream file4("point4.txt");
	ofstream file5("energy.txt");	

	file1 << "";
	file2 << "";
	file3 << "";
	file4 << "";
	file5 << "";

	file1.close();
	file2.close();
	file3.close();
	file4.close();
	file5.close();
}



//______________________________________________________________


void InitialConditions(double *x, double *y,double *z, double *u, double *v,double *w, double *uc, double *vc,double *wc)
{	double R1,R2,R3;
	R1 = 0.5*(unstretched + (1/sqrt(k))*RandG());
	R2 = RandG();
	R3 = RandG();

	

	for (int i=0;i<molecul;i++)
	{
		x[2*i] = R1*cos(2*Pi*R2)*cos(2*Pi*R3);//x
		y[2*i] = R1*cos(2*Pi*R2)*sin(2*Pi*R3);	//y
		z[2*i] = R1*sin(2*Pi*R2);//z
		
		x[2*i+1] = - x[2*i];
		y[2*i+1] = - y[2*i];
		z[2*i+1] = - z[2*i];

//		cout << x[2*i] <<" " <<y[2*i] <<" " <<z[2*i] <<"\n ";
//		cout << x[2*i+1] <<" " <<y[2*i+1] <<" " <<z[2*i+1] <<"\n ";


		u[2*i] = RandG();	//vx
		v[2*i] = RandG();	//vy
		w[2*i] = RandG();	//vz

		u[2*i+1] = RandG();	//vx
		v[2*i+1] = RandG();	//vy
		w[2*i+1] = RandG();	//vz
	}

	x[2] += length;
	x[3] += length;

	MiddleVelocity(u,v,w,uc,vc,wc);
	
	for (int i=0;i<atoms;i++)
	{
		u[i] = u[i] - uc[i/2];
		v[i] = v[i] - vc[i/2];
		w[i] = w[i] - wc[i/2];

	}


		u[2] += velocity;
		u[3] += velocity;
	
}

//______________________________________________________________