#include "vetale.h"


void Step1(double *x, double *y, double *z, double *u, double *v, double *w, double *ax, double *ay, double *az, double *up, double *vp,double *wp)
{
	for (int i = 0; i<atoms; i++)  
		{
			up[i] = u[i] + (h/2)*ax[i];
			vp[i] = v[i] + (h/2)*ay[i];
			wp[i] = w[i] + (h/2)*az[i];
		}
}

//______________________________________________________________
void Step2(double *x, double *y, double *z, double *u, double *v, double *w, double *ax, double *ay, double *az, double *up, double *vp,double *wp)
{
	for (int i = 0; i<atoms; i++)  
	{
		x[i] = x[i] + h*up[i];
		y[i] = y[i] + h*vp[i];
		z[i] = z[i] + h*wp[i];
	}

}
//______________________________________________________________

void Step3(double *x, double *y, double *z, double *u, double *v, double *w, double *ax, double *ay, double *az, double *up, double *vp,double *wp)
{	for (int i = 0; i<atoms; i++) 
	{
		u[i] = up[i] + (h/2)*ax[i];
		v[i] = vp[i] + (h/2)*ay[i];
		w[i] = wp[i] + (h/2)*az[i];
	}
}
//______________________________________________________________

void Acceleration(double *x, double *y, double *z,double *ax, double *ay, double *az)
{	
	ax[0] = InForce(0,0,1,x,y,z) - (OutForce1(0,0,2,x,y,z) - OutForce2(0,0,2,x,y,z) + OutForce1(0,0,3,x,y,z) - OutForce2(0,0,2,x,y,z));
	ay[0] = InForce(1,0,1,x,y,z) - (OutForce1(1,0,2,x,y,z) - OutForce2(1,0,2,x,y,z) + OutForce1(1,0,3,x,y,z) - OutForce2(1,0,2,x,y,z));
	az[0] = InForce(2,0,1,x,y,z) - (OutForce1(2,0,2,x,y,z) - OutForce2(2,0,2,x,y,z) + OutForce1(2,0,3,x,y,z) - OutForce2(2,0,2,x,y,z));

	ax[1] = InForce(0,1,0,x,y,z) - (OutForce1(0,1,2,x,y,z) - OutForce2(0,1,2,x,y,z) + OutForce1(0,1,3,x,y,z) - OutForce2(0,1,2,x,y,z));
	ay[1] = InForce(1,1,0,x,y,z) - (OutForce1(1,1,2,x,y,z) - OutForce2(1,1,2,x,y,z) + OutForce1(1,1,3,x,y,z) - OutForce2(1,1,2,x,y,z));
	az[1] = InForce(2,1,0,x,y,z) - (OutForce1(2,1,2,x,y,z) - OutForce2(2,1,2,x,y,z) + OutForce1(2,1,3,x,y,z) - OutForce2(2,1,2,x,y,z));

	ax[2] = InForce(0,2,3,x,y,z) - (OutForce1(0,2,0,x,y,z) - OutForce2(0,2,0,x,y,z) + OutForce1(0,2,1,x,y,z) - OutForce2(0,2,1,x,y,z)); 
	ay[2] = InForce(1,2,3,x,y,z) - (OutForce1(1,2,0,x,y,z) - OutForce2(1,2,0,x,y,z) + OutForce1(1,2,1,x,y,z) - OutForce2(1,2,1,x,y,z)); 
	az[2] = InForce(2,2,3,x,y,z) - (OutForce1(2,2,0,x,y,z) - OutForce2(2,2,0,x,y,z) + OutForce1(2,2,1,x,y,z) - OutForce2(2,2,1,x,y,z)); 

	ax[3] = InForce(0,3,2,x,y,z) - (OutForce1(0,3,0,x,y,z) - OutForce2(0,3,0,x,y,z) + OutForce1(0,3,1,x,y,z) - OutForce2(0,3,1,x,y,z));
	ay[3] = InForce(1,3,2,x,y,z) - (OutForce1(1,3,0,x,y,z) - OutForce2(1,3,0,x,y,z) + OutForce1(1,3,1,x,y,z) - OutForce2(1,3,1,x,y,z));
	az[3] = InForce(2,3,2,x,y,z) - (OutForce1(2,3,0,x,y,z) - OutForce2(2,3,0,x,y,z) + OutForce1(2,3,1,x,y,z) - OutForce2(2,3,1,x,y,z));
}

//______________________________________________________________


double InForce(int check, int i, int j, double *x,double *y, double *z) //check == 0, проекция на x, 1 -- на y.
{	
	if (check == 0)	return 2*k*(x[j]-x[i])*(VectorLenght(i,j,x,y,z) - unstretched)/VectorLenght(i,j,x,y,z);
	if (check == 1) return 2*k*(y[j]-y[i])*(VectorLenght(i,j,x,y,z) - unstretched)/VectorLenght(i,j,x,y,z);
	return 2*k*(z[j]-z[i])*(VectorLenght(i,j,x,y,z) - unstretched)/VectorLenght(i,j,x,y,z);
}

//______________________________________________________________

double OutForce1(int check, int i, int j, double *x,double *y, double *z)
{	
	if (check == 0)	return 48*(x[j]-x[i])/pow(VectorLenght(i,j,x,y,z),14);
	if (check == 1) return 48*(y[j]-y[i])/pow(VectorLenght(i,j,x,y,z),14);
	return 48*(z[j]-z[i])/pow(VectorLenght(i,j,x,y,z),14);
}

//______________________________________________________________

double OutForce2(int check, int i, int j, double *x,double *y, double *z)
{	
	if (check == 0)	return 24*(x[j]-x[i])/pow(VectorLenght(i,j,x,y,z),8);
	if (check == 1) return 24*(y[j]-y[i])/pow(VectorLenght(i,j,x,y,z),8);
	return 24*(z[j]-z[i])/pow(VectorLenght(i,j,x,y,z),8);
}


void Step(double *x, double *y, double *z, double *u, double *v, double *w, double *ax, double *ay, double *az, double *up, double *vp,double *wp)
{	
	for (int i = 0; i<atoms; i++)  
		{	u[i] = up[i]; v[i] = vp[i]; w[i] = wp[i]; 
			up[i] = up[i] + (h)*ax[i];
			vp[i] = vp[i] + (h)*ay[i];
			wp[i] = wp[i] + (h)*az[i];
		}
		for (int i = 0; i<atoms; i++)  
		{
			x[i] = x[i] + (h)*up[i];
			y[i] = y[i] + (h)*vp[i];
			z[i] = z[i] + (h)*wp[i];
		}
		for (int i=0;i<atoms;i++)
		{
			u[i] = 0.5*(up[i] + u[i]);
			v[i] = 0.5*(vp[i] + v[i]);
			w[i] = 0.5*(wp[i] + w[i]);
		}
}

