#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string>

	#define atoms 4 // кол. атомов
	#define molecul 2 // кол. молекул
	#define h  0.00043 // шаг интегрирования
	#define k  563292.214525 // внутренний энергетический коэффициент молекулы
	#define unstretched 2.49495628783 // длина связи в молекуле в состоянии покоя
	#define rang 1000 // количество разлинчых начальных условий
	#define beta 0.0408 // безразмерный коэффициент в Максвелловском распределении
 	#define Pi 3.1415926
	#define velocity -10 // скорость по оси x молекулы 2 в начальный момент времени
	#define length 50 // растояние между центром масс второй молекулы и первой молекулы при t = 1
	#define file_koord	"velocities.txt"
	#define file_final "final.txt"

using namespace std;


void Start(); // очищение/создание файлов
void InitialConditions(double *x, double *y,double *z, double *u, double *v,double *w,double *uc,double *vc,double *wc); // задание начальных координат
void Acceleration(double *x, double *y, double *z, double *ax, double *ay, double *az); // задание ускорений

double Energy(double *x,double *y, double *z, double *u,double *v, double *w); // Подсчет полной энергии
double CentreMassEnergy(double *u,double *v, double *w,double *uc,double *vc,double *wc);// Подсчет кинетической энергии молекулы
double CheckEnergy(double *mistake, double step); //Средняя полная энергия ± погрешность (mistake)
double PotentialEnergy(double *x, double *y, double *z);

double OutForce1(int check, int i, int j, double *x,double *y, double *z); 
double OutForce2(int check, int i, int j, double *x,double *y, double *z);
double InForce(int check, int i, int j, double *x,double *y, double *z);

//INTEGRATION
void Step1(double *x, double *y, double *z, double *u, double *v, double *w, double *ax, double *ay, double *az, double *up, double *vp,double *wp);
void Step2(double *x, double *y, double *z, double *u, double *v, double *w, double *ax, double *ay, double *az, double *up, double *vp,double *wp);
void Step3(double *x, double *y, double *z, double *u, double *v, double *w, double *ax, double *ay, double *az, double *up, double *vp,double *wp);

void Step(double *x, double *y, double *z, double *u, double *v, double *w, double *ax, double *ay, double *az, double *up, double *vp,double *wp);

//Вывод
void OutInFile(double *x, double *y,double *z,double *u, double *v,double *w);
void OutEnergy(double *x, double *y,double *z,double *u, double *v,double *w);
void OutVTK(double *x, double *y,double *z,int number);
void OutTheFinalEnergy(double step, double *x, double *y, double *z, double *u,double *v, double *w,double *uc,double *vc,double *wc);

void OutTheFinal(double step, double *x, double *y, double *z, double *u,double *v, double *w,double *uc,double *vc,double *wc, double *middle, double *r);
void OutSpecial(int i, double *x, double *y, double *z, double *u,double *v, double *w);


void MiddleVelocity(double *u,double *v,double *w,double *uc,double *vc,double *wc);
double VectorLenght(int i, int j, double *x, double *y, double *z);
double RandG();


double kub(double *x,double *y,double *z);

void Energ1_Out(int i,double *x,double *y, double *z, double *u,double *v, double *w);
double Energ1_Check(int i);