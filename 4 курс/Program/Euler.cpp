#include "headers/Euler.h"
/* определена в system.h */
void Euler(int vtk, double *x0, double *y0, double *z0, double *u0, double *v0, double *w0, double *x){

    double old[GREAT_DIM];
	double (*f[GREAT_DIM])(double*, double*);
	double dist;
	double T,V,H;
	int check_distanse = 0;
	int counter = 0;
	int answer;

	if (vtk == 1)
    	Create_folder("out");
	
	//printf("START Euler MD\n");
	Create_Initials_Euler(x,x0,y0,z0,u0,v0,w0, f);

	for (double t = 0; t < TIME_LIMIT; t+= h){	
		if ((vtk == 1)&&(counter %10 == 0)){
		//	printf("Print vtk: \t %e;\n",t );
			Out_vtk("out",counter, x);
		}
		for (int i=0; i< GREAT_DIM; i++) old[i] = x[i];
		Newton(x,old,f);
		counter++;
		check_distanse = check_dist(x, &dist);
		if (check_distanse == 1)
			break;
	}

/*	T = KineticEnergy(x);
	V = PotentialEnergy(x); 
	H = FullEnergy(x);
	printf(" Finish calculations;\n molecules fly on the distanse: %e\n", sqrt(dist));
	printf(" Euler T = %e \t V = %e \n T/V = %e \t H = %e \n ",T,V, T/V, H );
*/
}