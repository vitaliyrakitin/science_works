#include "headers/LeapFrog.h"

void LeapFrog(int vtk, double *x0, double *y0, double *z0, double *u0, double *v0, double *w0, double *res){
	
	double x[DIM],   y[DIM],  z[DIM];
	double u[DIM],   v[DIM],  w[DIM];
	double ax[DIM], ay[DIM], az[DIM];
	double up[DIM], vp[DIM], wp[DIM];
	double T,V,H;
	int step=0;
	int check_distanse;
	double dist;
	//printf("START LF\n");
	if (vtk == 1)
    	Create_folder("out_LF");

	for (int i=0;i< DIM;i++){
		x[i] = x0[i]; 
		y[i] = y0[i]; 
		z[i] = z0[i]; 
		u[i] = u0[i]; 
		v[i] = v0[i]; 
		w[i] = w0[i]; 
	}

	Acceleration(x,y,z,ax,ay,az);

	for(int j = 0; j<DIM; j++)
	{
	 	up[j] = u[j]; vp[j] = v[j]; wp[j] = w[j];
	 //	cout << u[j] << " " << v[j] << " "<<w[j] << "\n";
	}
	for (double t = 0; t < TIME_LIMIT; t+= h)
	{	
		if ((step%10 == 0)&&(vtk == 1))
			Out_vtk("out_LF", step, x, y, z);
		Acceleration(x,y,z,ax,ay,az);
		Step(x,y,z,u,v,w,ax,ay,az,up,vp,wp);
		step++;
		check_distanse = check_dist(x,y,z, &dist);

		if (check_distanse == 1)
			break;
	}
	for (int i=0;i< GREAT_DIM; i+= 6){
		res[i] = x[i/6];
		res[i+1] = u[i/6];
		res[i+2] = y[i/6];
		res[i+3] = v[i/6];
		res[i+4] = z[i/6];
		res[i+5] = w[i/6];
	}
		/*
		printf(" Finish calculations;\n molecules fly on the distanse: %e\n", sqrt(dist));
		T = KineticEnergy(u,v,w);
		V = PotentialEnergy(x,y,z); 
		H = FullEnergy(x,y,z,u,v,w);
		printf(" LeapFrog T = %e \t V = %e \n T/V = %e \t H = %e \n ",T,V, T/V, H );
	*/
}


void Acceleration(double *x, double *y, double *z,double *ax, double *ay, double *az)
{	
	ax[0] = fvx1(x,ax,y,ay,z,az);
	ay[0] = fvy1(x,ax,y,ay,z,az);
	az[0] = fvz1(x,ax,y,ay,z,az);

	// 2 атом
	ax[1] = fvx2(x,ax,y,ay,z,az);
	ay[1] = fvy2(x,ax,y,ay,z,az);
	az[1] = fvz2(x,ax,y,ay,z,az);

	ax[2] = fvx3(x,ax,y,ay,z,az);
	ay[2] = fvy3(x,ax,y,ay,z,az);
	az[2] = fvz3(x,ax,y,ay,z,az);

	// 2 атом
	ax[3] = fvx4(x,ax,y,ay,z,az);
	ay[3] = fvy4(x,ax,y,ay,z,az);
	az[3] = fvz4(x,ax,y,ay,z,az);
}

void Step(double *x, double *y, double *z, double *u, double *v, double *w, double *ax, double *ay, double *az, double *up, double *vp,double *wp)
{	
	for (int i = 0; i<DIM; i++)  
		{	u[i] = up[i]; v[i] = vp[i]; w[i] = wp[i]; 
			up[i] = up[i] + (h)*ax[i];
			vp[i] = vp[i] + (h)*ay[i];
			wp[i] = wp[i] + (h)*az[i];
		}
		for (int i = 0; i<DIM; i++)  
		{
			x[i] = x[i] + (h)*up[i];
			y[i] = y[i] + (h)*vp[i];
			z[i] = z[i] + (h)*wp[i];
		}
		for (int i=0;i<DIM;i++)
		{
			u[i] = 0.5*(up[i] + u[i]);
			v[i] = 0.5*(vp[i] + v[i]);
			w[i] = 0.5*(wp[i] + w[i]);
		}
}

