 #include "headers/Euler.h"


//#define eps1 1e-15
#define eps 1e-6
#define check_eps 10e-7


/* Поиск решения системы уравнений метода Ньютона */
void Newton( double x[GREAT_DIM], double prev[GREAT_DIM], double (*f[GREAT_DIM])(double*, double*)){
	matrix A(0);
	matrix B(0);
	matrix C(0);
	double y[GREAT_DIM];
//	int counter = 0;
	
	A = Jacobian(x, prev, f);
	B = Vector_Func(x, prev, f);
	C = C.ArrayToMatrix(x, GREAT_DIM).transpose();
	A = C - A.Back() * B;	

	toArray(y, A);

	while( norm(x,prev,f) > check_eps){
//		counter++;
		for (int i = 0; i< GREAT_DIM; i++) x[i] = y[i];

		A = Jacobian(x, prev, f); 
		B = Vector_Func(x, prev, f);
		C = C.ArrayToMatrix(x, GREAT_DIM).transpose();
		A = C - A.Back() * B;
		toArray(y, A);
	}
	for (int i = 0; i< GREAT_DIM; i++) x[i] = y[i];

}

/* подсчёт матрицы Якоби */
matrix Jacobian(double x[GREAT_DIM], double prev[GREAT_DIM], double (*f[GREAT_DIM])(double*, double*)){

	matrix A(GREAT_DIM);
	for (int i = 0; i < GREAT_DIM; i++)
		for (int j = 0; j < GREAT_DIM;j++){
	//	printf("______________________________________________________\n" );
	//	printf("FUNC NUMBER: \t %d \n", i );
			A.matr[i][j] = count_derivative(j,x,prev,f[i]);
			//printf("%e\n",A.matr[i][j] );
		}

	return A;
}

/* возвращает максимальное значение вектора функций */
double norm(double x[GREAT_DIM], double prev[GREAT_DIM], double (*f[GREAT_DIM])(double*, double*)){
	double max = 0;
	double perm = 0;
	for (int i = 0;i < GREAT_DIM; i++){
		perm = huabs((f[i])(x,prev));
		if (perm > max) max = perm;
	}
	return perm;
}
  
/* модуль */
double huabs(double a){
	if (a > 0) return a;
	else return -a;
}

/* считаем производную */
double count_derivative(int num, double x[GREAT_DIM], double prev[GREAT_DIM], double  (*f)(double*, double*)){

	double New[GREAT_DIM];
	for (int i = 0; i<GREAT_DIM; i++){
		if (i == num) New[i] = x[i] + eps;
		else New[i] = x[i];
	}
	double result = (f(New,prev) - f(x,prev))/eps;

	return result;
}

/* создаём вектор функцию в точке */
matrix Vector_Func(double x[GREAT_DIM], double prev[GREAT_DIM], double (*f[GREAT_DIM])(double*, double*)){

	matrix A(GREAT_DIM, 1);
		for (int i = 0; i < GREAT_DIM; i++)
				A.matr[i][0] = (f[i])(x,prev);
	return A;
}

/* преобразуем матрицу в массив */
void toArray(double x[GREAT_DIM], matrix A){
	if ((A.sto == GREAT_DIM)&&(A.str == 1)){
		for (int i = 0; i < GREAT_DIM; i++) x[i] = A.matr[0][i];
	}
	else if ((A.sto == 1)&&(A.str == GREAT_DIM)){
		for (int i = 0; i < GREAT_DIM; i++) x[i] = A.matr[i][0];
	}
	else {printf("ERROR! Wrong toArray!\n"); throw(1);}
}
