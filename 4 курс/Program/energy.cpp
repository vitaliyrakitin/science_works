#include "headers/system.h"

double PotentialEnergy(double *koord){
	double x[DIM], y[DIM], z[DIM];
	for (int i=0; i< GREAT_DIM; i+= 6){
		 x[i/6] = koord[i + 0];
		 y[i/6] = koord[i + 2];
		 z[i/6] = koord[i + 4];
	}
	return PotentialEnergy(x,y,z);
}
//______________________________________________________________
double KineticEnergy(double *koord){
	double u[DIM],v[DIM],w[DIM];
	for (int i=0; i< GREAT_DIM; i+= 6){
		u[i/6] = koord[i + 1];
		v[i/6] = koord[i + 3];
		w[i/6] = koord[i + 5];
	}
	return KineticEnergy(u,v,w);
}

//______________________________________________________________
double FullEnergy(double *koord){
	double x[DIM], y[DIM], z[DIM];	
	double u[DIM],v[DIM],w[DIM];
	for (int i=0; i< GREAT_DIM; i+= 6){
		 x[i/6] = koord[i + 0];
		 u[i/6] = koord[i + 1];
		 y[i/6] = koord[i + 2];
		 v[i/6] = koord[i + 3];
		 z[i/6] = koord[i + 4];
		 w[i/6] = koord[i + 5];
	}
	return FullEnergy(x,y,z,u,v,w);
}

//______________________________________________________________

double PotentialEnergy(double *x,double *y, double *z)
{	
	double V[6];
	double result = 0.0;
	double R = 1.0;
	
	for (int i = 0;i < molecul; i++){
		R = sqrt(Distance2( x[2*i],  y[2*i],  z[2*i], x[2*i+1],  y[2*i+1], z[2*i+1])) - unstretched[i];
		V[i] = k[i] * R * R;
	}

	R = 1.0;
	for (int i=0; i<3; i++)
		R *= Distance2( x[0],  y[0],  z[0], x[2],  y[2], z[2]);
	V[2] = - 4.0 * sigma[0][2] / R;
	R = R * R;
	V[2] += 4.0 * sigma[0][2] * sigma[0][2] / R;

	R = 1.0;
	for (int i=0; i<3; i++)
		R *= Distance2( x[0],  y[0],  z[0], x[3],  y[3], z[3]);
	V[3] = - 4.0 * sigma[0][3] / R;
	R = R * R;
	V[3] += 4.0 * sigma[0][3] * sigma[0][3] / R;

	R = 1.0;
	for (int i=0; i<3; i++)
		R *= Distance2( x[1],  y[1],  z[1], x[2],  y[2], z[2]);
	V[4] = - 4.0 * sigma[1][2] / R;
	R = R * R;
	V[4] += 4.0 * sigma[1][2] * sigma[1][2] / R;

	R = 1.0;
	for (int i=0; i<3; i++)
		R *= Distance2( x[1],  y[1],  z[1], x[3],  y[3], z[3]);
	V[5] = - 4.0 * sigma[1][3] / R;
	R = R * R;
	V[5] += 4.0 * sigma[1][3] * sigma[1][3] / R;


	for (int i=0;i < 6;i++)
		result += V[i];

	return result;
	
}

//______________________________________________________________

double KineticEnergy(double *u,double *v, double *w)
{	double T = 0.0;
	double uc[molecul], vc[molecul], wc[molecul];

	MiddleVelocity(u,v,w,uc,vc,wc);

	for (int i=0;i<molecul;i++) T += (m[i*2]+m[i*2+1])*(uc[i] * uc[i] + vc[i] * vc[i] + wc[i] * wc[i])/2 ;

	return T;
}

//______________________________________________________________

double FullEnergy(double *x, double *y, double *z, double *u, double *v, double *w){
	double T, V;
	T = AtomicKineticEnergy(u,v,w);
	V = PotentialEnergy(x,y,z);
	return T + V;
}




//______________________________________________________________


double AtomicKineticEnergy(double *u,double *v, double *w)
{	double T = 0.0;

	for (int i=0;i<DIM;i++) T += (m[i])*(u[i] * u[i] + v[i] * v[i] + w[i] * w[i])/2 ;

	return T;
}
double AtomicKineticEnergy(double *koord){
	double u[DIM],v[DIM],w[DIM];
	for (int i=0; i< GREAT_DIM; i+= 6){
		u[i/6] = koord[i + 1];
		v[i/6] = koord[i + 3];
		w[i/6] = koord[i + 5];
	}
	return AtomicKineticEnergy(u,v,w);
}


