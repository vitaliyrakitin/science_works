 /*
 * Тут хранятся функции, которые мы интегрируем
 * получены аналитическим путём
 *
 *
 * koeff = {eps_(i,j), b_{i,j}}
 * sigma = {sigma_(i,j)}
 *
 *
 * Для начала рассмотрим столкновение двух ОДИНАКОВЫХ молекул с ОДИНАКОВЫМИ атомами
 * Всё обезразмерено!
 *
 * все функции определены в func_system.h
 */ 

#include "headers/system.h"
/* собственно функции */
double fx1(double *x, double *vx, double *y, double *vy, double *z, double *vz){
	return vx[0];
}

double fvx1(double *x, double *vx, double *y, double *vy, double *z, double *vz){
	return (Upr(x, 0, 1, x,y,z) + LD(x, 0, 2, x,y,z) + LD(x, 0, 3, x,y,z))/m[0];
}

double fy1(double *x, double *vx, double *y, double *vy, double *z, double *vz){
	return vy[0];
}

double fvy1(double *x, double *vx, double *y, double *vy, double *z, double *vz){
	return (Upr(y, 0, 1, x,y,z) + LD(y, 0, 2, x,y,z) + LD(y, 0, 3, x,y,z))/m[0];
}

double fz1(double *x, double *vx, double *y, double *vy, double *z, double *vz){
	return vz[0];
}

double fvz1(double *x, double *vx, double *y, double *vy, double *z, double *vz){
	return  (Upr(z, 0, 1, x,y,z) + LD(z, 0, 2, x,y,z) + LD(z, 0, 3, x,y,z))/m[0];
}


double fx2(double *x, double *vx, double *y, double *vy, double *z, double *vz){
	return vx[1];
}

double fvx2(double *x, double *vx, double *y, double *vy, double *z, double *vz){
	return (Upr(x, 1, 0, x,y,z) + LD(x, 1, 2, x,y,z) + LD(x, 1, 3, x,y,z))/m[1];
}

double fy2(double *x, double *vx, double *y, double *vy, double *z, double *vz){
	return vy[1];
}

double fvy2(double *x, double *vx, double *y, double *vy, double *z, double *vz){
	return (Upr(y, 1, 0, x,y,z) + LD(y, 1, 2, x,y,z) + LD(y, 1, 3, x,y,z))/m[0];
}

double fz2(double *x, double *vx, double *y, double *vy, double *z, double *vz){
	return vz[1];
}

double fvz2(double *x, double *vx, double *y, double *vy, double *z, double *vz){
	return  (Upr(z, 1, 0, x,y,z) + LD(z, 1, 2, x,y,z) + LD(z, 1, 3, x,y,z))/m[0];
}




double fx3(double *x, double *vx, double *y, double *vy, double *z, double *vz){
	return vx[2];
}

double fvx3(double *x, double *vx, double *y, double *vy, double *z, double *vz){
	return (Upr(x, 2, 3, x,y,z) + LD(x, 2, 0, x,y,z) + LD(x, 2, 1, x,y,z))/m[1];
}

double fy3(double *x, double *vx, double *y, double *vy, double *z, double *vz){
	return vy[2];
}

double fvy3(double *x, double *vx, double *y, double *vy, double *z, double *vz){
	return (Upr(y, 2, 3, x,y,z) + LD(y, 2, 0, x,y,z) + LD(y, 2, 1, x,y,z))/m[0];
}

double fz3(double *x, double *vx, double *y, double *vy, double *z, double *vz){
	return vz[2];
}

double fvz3(double *x, double *vx, double *y, double *vy, double *z, double *vz){
	return  (Upr(z, 2, 3, x,y,z) + LD(z, 2, 0, x,y,z) + LD(z, 2, 1, x,y,z))/m[0];
}


double fx4(double *x, double *vx, double *y, double *vy, double *z, double *vz){
	return vx[3];
}

double fvx4(double *x, double *vx, double *y, double *vy, double *z, double *vz){
	return (Upr(x, 3, 2, x,y,z) + LD(x, 3, 0, x,y,z) + LD(x, 3, 1, x,y,z))/m[1];
}

double fy4(double *x, double *vx, double *y, double *vy, double *z, double *vz){
	return vy[3];
}

double fvy4(double *x, double *vx, double *y, double *vy, double *z, double *vz){
	return (Upr(y, 3, 2, x,y,z) + LD(y, 3, 0, x,y,z) + LD(y, 3, 1, x,y,z))/m[0];
}

double fz4(double *x, double *vx, double *y, double *vy, double *z, double *vz){
	return vz[3];
}

double fvz4(double *x, double *vx, double *y, double *vy, double *z, double *vz){
	return  (Upr(z, 3, 2, x,y,z) + LD(z, 3, 0, x,y,z) + LD(z, 3, 1, x,y,z))/m[0];
}


/* взаимодействие полученное из потенциала Леннорда-Джонса для i -- j атомов РАЗНЫХ молекул */
double LD(double *koord, int i, int j, double *x, double *y, double *z){
	double result = 0.0;
	double length2 = Distance2(x[i],y[i],z[i],x[j],y[j],z[j]); // квадрат длины вектора

	result = LD_Step(0,koord,i,j,length2) -  LD_Step(1,koord,i,j,length2);

	return result;
}

double LD_Step(int step_number, double *koord, int i, int j, double length2){
	double denominator = 1.0; // знаменатель
	double result = 1.0;

	for (int i = 0; i < 4 ; i++) denominator *= length2;	// length2 в 4 степени

	if (step_number == 0){
		for (int i = 0; i < 3 ; i++) denominator *= length2;	//length2 в 7 степени
		result *= 48.0 * koeff[i][j] *  sigma[i][j] * sigma[i][j] * (koord[i] - koord[j]) / denominator;
	}		
	else{
		result *= 24.0 * koeff[i][j] *  sigma[i][j] * (koord[i] - koord[j]) / denominator;
	}

	return result;	
}

/* взаимодействие полученное из упругого потенциала для i -- j атомов одной молекул */
double Upr(double *koord, int i, int j, double *x, double *y, double *z){
	double result = 2.0 * koeff[i][j];

	double length2 = Distance2(x[i],y[i],z[i],x[j],y[j],z[j]); // квадрат длины вектора
	double length = sqrt(length2); //  длина вектора

	result *= (koord[j] - koord[i]) * (length - relaxed[i][j]) / length;
	return result;
}
