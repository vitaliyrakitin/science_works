/*
 * Функции для неявного Эйлера
 * объявлены в functions.h
 */

#include "headers/system.h"
double (*fp[GREAT_DIM]) (double *x, double *vx, double *y, double *vy, double *z, double *vz) =	{	fx1, fvx1, fy1, fvy1, fz1, fvz1, 
																									fx2, fvx2, fy2, fvy2, fz2, fvz2, 
																									fx3, fvx3, fy3, fvy3, fz3, fvz3,
																									fx4, fvx4, fy4, fvy4, fz4, fvz4	};

double function(int number, double koord[GREAT_DIM], double koord1[GREAT_DIM]){
	double x[DIM];
	double vx[DIM];
	double y[DIM];
	double vy[DIM];
	double z[DIM];
	double vz[DIM];
	double x1[DIM];
	double vx1[DIM];
	double y1[DIM];
	double vy1[DIM];
	double z1[DIM];
	double vz1[DIM];

	double result;
	for (int i=0; i< GREAT_DIM; i+= 6){
		 x[i/6] = koord[i + 0];
		vx[i/6] = koord[i + 1];
		 y[i/6] = koord[i + 2];
		vy[i/6] = koord[i + 3];
		 z[i/6] = koord[i + 4];
		vz[i/6] = koord[i + 5];
	}

	for (int i=0; i< GREAT_DIM; i+= 6){
		 x1[i/6] = koord1[i + 0];
		vx1[i/6] = koord1[i + 1];
		 y1[i/6] = koord1[i + 2];
		vy1[i/6] = koord1[i + 3];
		 z1[i/6] = koord1[i + 4];
		vz1[i/6] = koord1[i + 5];
	}	

	result = -koord[number] + koord1[number] + h*(fp[number](x,vx,y,vy,z,vz) + fp[number](x1,vx1,y1,vy1,z1,vz1))/2;

	return result; 
}

double f1(double x[GREAT_DIM], double y[GREAT_DIM]){
	return function(0,x,y);
}
double f2(double x[GREAT_DIM], double y[GREAT_DIM]){
	return function(1,x,y);
}
double f3(double x[GREAT_DIM], double y[GREAT_DIM]){
	return function(2,x,y);
}
double f4(double x[GREAT_DIM], double y[GREAT_DIM]){
	return function(3,x,y);
}
double f5(double x[GREAT_DIM], double y[GREAT_DIM]){
	return function(4,x,y);
}
double f6(double x[GREAT_DIM], double y[GREAT_DIM]){
	return function(5,x,y);
}
double f7(double x[GREAT_DIM], double y[GREAT_DIM]){
	return function(6,x,y);
}
double f8(double x[GREAT_DIM], double y[GREAT_DIM]){
	return function(7,x,y);
}
double f9(double x[GREAT_DIM], double y[GREAT_DIM]){
	return function(8,x,y);
}
double f10(double x[GREAT_DIM], double y[GREAT_DIM]){
	return function(9,x,y);
}
double f11(double x[GREAT_DIM], double y[GREAT_DIM]){
	return function(10,x,y);
}
double f12(double x[GREAT_DIM], double y[GREAT_DIM]){
	return function(11,x,y);
}
double f13(double x[GREAT_DIM], double y[GREAT_DIM]){
	return function(12,x,y);
}
double f14(double x[GREAT_DIM], double y[GREAT_DIM]){
	return function(13,x,y);
}
double f15(double x[GREAT_DIM], double y[GREAT_DIM]){
	return function(14,x,y);
}
double f16(double x[GREAT_DIM], double y[GREAT_DIM]){
	return function(15,x,y);
}
double f17(double x[GREAT_DIM], double y[GREAT_DIM]){
	return function(16,x,y);
}
double f18(double x[GREAT_DIM], double y[GREAT_DIM]){
	return function(17,x,y);
}
double f19(double x[GREAT_DIM], double y[GREAT_DIM]){
	return function(18,x,y);
}
double f20(double x[GREAT_DIM], double y[GREAT_DIM]){
	return function(19,x,y);
}
double f21(double x[GREAT_DIM], double y[GREAT_DIM]){
	return function(20,x,y);
}
double f22(double x[GREAT_DIM], double y[GREAT_DIM]){
	return function(21,x,y);
}
double f23(double x[GREAT_DIM], double y[GREAT_DIM]){
	return function(22,x,y);
}
double f24(double x[GREAT_DIM], double y[GREAT_DIM]){
	return function(23,x,y);
}