#include "system.h"
#include "matrix.h"
void Create_Initials_Euler(double x[GREAT_DIM],double *x0, double *y0, double *z0, double *u0, double *v0, double *w0, double (*f[GREAT_DIM])(double*, double*));

void Newton( double x[GREAT_DIM], double prev[GREAT_DIM], double (*f[GREAT_DIM])(double*, double*));

matrix Jacobian(double x[GREAT_DIM], double prev[GREAT_DIM], double (*f[GREAT_DIM])(double*, double*));
double count_derivative(int num, double x[GREAT_DIM],double prev[GREAT_DIM], double (*f)(double*,double*));
matrix Vector_Func(double x[GREAT_DIM], double prev[GREAT_DIM],double (*f[GREAT_DIM])(double*, double*));
void toArray(double x[GREAT_DIM], matrix A);
double norm(double x[GREAT_DIM],double prev[GREAT_DIM], double (*f[GREAT_DIM])(double*, double*));
double huabs(double a);

void Create_Initials(double x[GREAT_DIM], double (*f[GREAT_DIM])(double*, double*));

