#include "system.h"

void Step(double *x, double *y, double *z, double *u, double *v, double *w, double *ax, double *ay, double *az, double *up, double *vp,double *wp);
void Acceleration(double *x, double *y, double *z,double *ax, double *ay, double *az);
void LeapFrog(int vtk);

double RandG();