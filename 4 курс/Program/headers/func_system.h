/* взаимодействие полученное из потенциала Леннорда-Джонса для i -- j атомов РАЗНЫХ молекул */
double LD(double *koord, int i, int j, double *x, double *y, double *z);
double LD_Step(int step_number, double *koord, int i, int j, double length2);
/* взаимодействие полученное из упругого потенциала для i -- j атомов одной молекул */
double Upr(double *koord, int i, int j, double *x, double *y, double *z);


/* Функции */

double fx1(double *x, double *vx, double *y, double *vy, double *z, double *vz);
double fvx1(double *x, double *vx, double *y, double *vy, double *z, double *vz);
double fy1(double *x, double *vx, double *y, double *vy, double *z, double *vz);
double fvy1(double *x, double *vx, double *y, double *vy, double *z, double *vz);
double fz1(double *x, double *vx, double *y, double *vy, double *z, double *vz);
double fvz1(double *x, double *vx, double *y, double *vy, double *z, double *vz);

double fx2(double *x, double *vx, double *y, double *vy, double *z, double *vz);
double fvx2(double *x, double *vx, double *y, double *vy, double *z, double *vz);
double fy2(double *x, double *vx, double *y, double *vy, double *z, double *vz);
double fvy2(double *x, double *vx, double *y, double *vy, double *z, double *vz);
double fz2(double *x, double *vx, double *y, double *vy, double *z, double *vz);
double fvz2(double *x, double *vx, double *y, double *vy, double *z, double *vz);

double fx3(double *x, double *vx, double *y, double *vy, double *z, double *vz);
double fvx3(double *x, double *vx, double *y, double *vy, double *z, double *vz);
double fy3(double *x, double *vx, double *y, double *vy, double *z, double *vz);
double fvy3(double *x, double *vx, double *y, double *vy, double *z, double *vz);
double fz3(double *x, double *vx, double *y, double *vy, double *z, double *vz);
double fvz3(double *x, double *vx, double *y, double *vy, double *z, double *vz);

double fx4(double *x, double *vx, double *y, double *vy, double *z, double *vz);
double fvx4(double *x, double *vx, double *y, double *vy, double *z, double *vz);
double fy4(double *x, double *vx, double *y, double *vy, double *z, double *vz);
double fvy4(double *x, double *vx, double *y, double *vy, double *z, double *vz);
double fz4(double *x, double *vx, double *y, double *vy, double *z, double *vz);
double fvz4(double *x, double *vx, double *y, double *vy, double *z, double *vz);