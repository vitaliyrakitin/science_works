#include <string> 
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>


/* константы */
#define GREAT_DIM 24 // размерность системы уравнений
#define DIM 4 // количество атомов
#define h  0.00043 // шаг интегрирования
//#define h 1e-5
#define TIME_LIMIT 20.0
#define dist_0 10.0 // растояние между центром масс второй молекулы и первой молекулы при t = 1
#define molecul 2

	#define rang 1000 // количество разлинчых начальных условий
	#define beta 0.1155 // безразмерный коэффициент в Максвелловском распределении
// 	#define beta 0.0812
 	#define Pi 3.1415926
//	#define velocity -30 // скорость по оси x молекулы 2 в начальный момент времени

const  double unstretched[molecul] = {2.49495628783,2.49495628783};
const  double k[molecul] =  {563292.214525,563292.214525}; // внутренний энергетический коэффициент молекулы

/* korff -- для одной молекулы b_ij, для разных eps */
const  double koeff[DIM][DIM] = {{0.0, 563292.214525, 1.0, 1.0 },{563292.214525, 0.0, 1.0, 1.0},{ 1.0, 1.0, 0.0, 563292.214525},{1.0, 1.0, 563292.214525, 0.0}};

/* безразмерный сигма */ 
const  double sigma[DIM][DIM] = {{0.0, 0.0, 1.0, 1.0},{0.0, 0.0, 1.0, 1.0},{1.0, 1.0, 0.0, 0.0},{1.0, 1.0, 0.0, 0.0}};

/* расстояние между атомами в нерастяженном состоянии */
const  double relaxed[DIM][DIM] = {{0.0,2.49495628783, 0.0, 0.0},{2.49495628783, 0.0, 0.0, 0.0},{0.0, 0.0, 0.0, 2.49495628783},{0.0, 0.0, 2.49495628783, 0.0}};
//const  double relaxed[DIM][DIM] = {{0.0,1.0, 0.0, 0.0},{1.0, 0.0, 0.0, 0.0},{0.0, 0.0, 0.0, 1.0},{0.0, 0.0, 1.0, 0.0}};

/* масса каждого атома */
const  double m[DIM] = {1.0, 1.0, 1.0, 1.0};

#include "func_system.h"
#include "functions.h"


using namespace std;
void InitialConditions(double, double *x, double *y,double *z, double *u, double *v,double *w);


/* out.cpp */
void Out_vtk(string folder, int num, double x[GREAT_DIM]); 
void Create_folder(string name); 
void Out_vtk(string folder, int num, double x[DIM], double y[DIM], double z[DIM]);
void Out_All(string folder, int num, double x[GREAT_DIM]);
void Out_All(string folder, int num, double x[DIM], double y[DIM], double z[DIM],double u[DIM], double v[DIM], double w[DIM]);




/* system.cpp */
double Distance2(double x, double y, double z, double x1, double y1, double z1);
int check_dist(double koord[GREAT_DIM], double *);
int check_dist(double x[DIM], double y[DIM], double z[DIM], double *result);
void MiddleVelocity(double *u,double *v,double *w,double *uc,double *vc,double *wc);

/* Euler.cpp */
void Euler(int vtk,double *x0, double *y0, double *z0, double *u0, double *v0, double *w0, double *);
void LeapFrog(int vtk,double *x0, double *y0, double *z0, double *u0, double *v0, double *w0, double *);

/* Energy.cpp */
double PotentialEnergy(double *x,double *y, double *z);
double KineticEnergy(double *u,double *v, double *w);
double FullEnergy(double *x, double *y, double *z, double *u, double *v, double *w);

double PotentialEnergy(double *koord);
double KineticEnergy(double *koord);
double FullEnergy(double *koord);

double AtomicKineticEnergy(double *u,double *v, double *w);
double AtomicKineticEnergy(double *koord);


void Out_Energy(string folder, int num, double T, double V);

