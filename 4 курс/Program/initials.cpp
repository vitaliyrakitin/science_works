#include "headers/system.h"
double RandG()
{ 
 return sqrt(-log(((double)rand())/(RAND_MAX)))/beta*sin(2.0*Pi*((double)rand())/(RAND_MAX));
}


void InitialConditions(double velocity, double *x, double *y,double *z, double *u, double *v,double *w)
{	
	srand( (unsigned)time( NULL ) ); //инит только 1 раз

	double R1, R2,R3;
	double uc[molecul], vc[molecul], wc[molecul];
	for (int i=0;i<molecul;i++)
	{
		R1 = 0.5*(unstretched[i] + (1/sqrt(k[i]))*RandG());
		R2 = RandG();
		R3 = RandG();

		x[2*i] = R1*cos(2*Pi*R2)*cos(2*Pi*R3);//x
		y[2*i] = R1*cos(2*Pi*R2)*sin(2*Pi*R3);	//y
		z[2*i] = R1*sin(2*Pi*R2);//z
		
		x[2*i+1] = - x[2*i];
		y[2*i+1] = - y[2*i];
		z[2*i+1] = - z[2*i];

//		cout << x[2*i] <<" " <<y[2*i] <<" " <<z[2*i] <<"\n ";
//		cout << x[2*i+1] <<" " <<y[2*i+1] <<" " <<z[2*i+1] <<"\n ";


		u[2*i] = RandG();	//vx
		v[2*i] = RandG();	//vy
		w[2*i] = RandG();	//vz

		u[2*i+1] = RandG();	//vx
		v[2*i+1] = RandG();	//vy
		w[2*i+1] = RandG();	//vz
	}

	x[2] += dist_0;
	x[3] += dist_0;

	MiddleVelocity(u,v,w,uc,vc,wc);
	
	for (int i=0;i<DIM;i++)
	{
		u[i] = u[i] - uc[i/2];
		v[i] = v[i] - vc[i/2];
		w[i] = w[i] - wc[i/2];

	}
	u[2] += velocity;
	u[3] += velocity;
	
}

//______________________________________________________________