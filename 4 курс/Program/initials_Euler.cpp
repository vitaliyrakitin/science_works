#include "headers/Euler.h"
 void Create_Initials_Euler(double x[GREAT_DIM], double *x0, double *y0, double *z0, double *u0, double *v0, double *w0, double (*f[GREAT_DIM])(double*, double*)){
	f[0] = f1; 
	f[1] = f2; 
	f[2] = f3; 
	f[3] = f4; 
	f[4] = f5; 
	f[5] = f6; 
	f[6] = f7; 
	f[7] = f8; 
	f[8] = f9; 
	f[9] = f10; 
	f[10] = f11; 
	f[11] = f12; 
	f[12] = f13; 
	f[13] = f14; 
	f[14] = f15; 
	f[15] = f16; 
	f[16] = f17; 
	f[17] = f18; 
	f[18] = f19; 
	f[19] = f20; 
	f[20] = f21; 
	f[21] = f22; 
	f[22] = f23; 
	f[23] = f24;

	x[0] = x0[0]; 	// x
	x[1] = u0[0]; 	// vx
	x[2] = y0[0]; 	// y
	x[3] = v0[0]; 	// vy
	x[4] = z0[0]; 	// z
	x[5] = w0[0]; 	// vz


	x[6] = x0[1]; 	// x
	x[7] = u0[1]; 	// vx
	x[8] = y0[1]; 	// y
	x[9] = v0[1]; 	// vy
	x[10] = z0[1]; 	// z
	x[11] = w0[1]; 	// vz
	

	x[12] = x0[2]; 	// x
	x[13] = u0[2]; 	// vx
	x[14] = y0[2]; 	// y
	x[15] = v0[2]; 	// vy
	x[16] = z0[2]; 	// z
	x[17] = w0[2]; 	// vz
	

	x[18] = x0[3]; 	// x
	x[19] = u0[3]; 	// vx
	x[20] = y0[3]; 	// y
	x[21] = v0[3]; 	// vy
	x[22] = z0[3]; 	// z
	x[23] = w0[3]; 	// vz
	
}