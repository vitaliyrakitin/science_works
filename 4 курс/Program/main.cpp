#include "headers/system.h"

int main(void){
    unsigned long int start, finish;
    double x[DIM],   y[DIM],  z[DIM];
	double u[DIM],   v[DIM],  w[DIM];
	double T,V,H;
	double res[GREAT_DIM];
    int counter = 0;
	unsigned long int time_Euler = 0; 
	unsigned long int time_LF = 0; 
	unsigned long int now = 0; 
	unsigned long int prew = 0; 
    char fn_init[300], fn_Euler[300], fn_LF[300];
    start = clock();
	prew = start;
        try{ 
                time_t now = time(0);
                sprintf(fn_init, "init%d", now);
                sprintf(fn_Euler, "Euler%d", now);
                sprintf(fn_LF, "LF%d", now);
		Create_folder(fn_init);
		Create_folder(fn_Euler);
		Create_folder(fn_LF);
		double i = 0;
	   	for (double i = 0; i < 30; i+= 0.5){
    		printf("Start calculation of V = %e\n", -10.0 - i);
    		for (int j=0;j<100;j++){
    			if (j %10 == 0) printf("\t%d%%\tOK\n", j );
				InitialConditions(-10-i, x,y,z,u,v,w);
//		    	T = AtomicKineticEnergy(u,v,w);
//				V = PotentialEnergy(x,y,z);
//				Out_Energy("init", counter, T,V);
				Out_All(fn_init, counter, x,y,z,u,v,w);
	   			Euler(0,x,y,z,u,v,w,res);
//	    		T = AtomicKineticEnergy(res);
//				V = PotentialEnergy(res); 
//				Out_Energy("Euler", counter, T,V);
				Out_All(fn_Euler, counter, res);
	   			now = clock();
				time_Euler += now - prew;


				prew = now;
				LeapFrog(0,x,y,z,u,v,w,res);
//		    	T = AtomicKineticEnergy(res);
//				V = PotentialEnergy(res); 
//				Out_Energy("LF", counter, T,V);
				Out_All(fn_LF, counter, res);
				now = clock();
				time_LF += now - prew;
			}
		//printf(" LeapFrog T = %e \t V = %e \n T/V = %e \t H = %e\t %e \n ",T,V, T/V, H, T+V );
			counter++;
		}
	}
	catch(int a){
		printf("Couht exception number: %d\n in experiment: %d",a, counter);
	}


    finish = clock();
    printf("Work-time: %e sec.\n", (double)(finish-start)/CLOCKS_PER_SEC);
    printf("LF-time: %e sec.\n", (double)(time_LF)/CLOCKS_PER_SEC);
    printf("Euler-time: %e sec.\n", (double)(time_Euler)/CLOCKS_PER_SEC);
	return 0;
}
