#include "headers/system.h"

/* создаёт папку с заданным именем, удаляет старую, объявлен в system.h*/
void Create_folder(string name){
	string str_1 = "rm -r -f " + name;
	string str_2 = "mkdir " + name;
	const char *text = str_1.c_str();
	const char *text1 = str_2.c_str();
	system(text);	
	system(text1);
}


void Out_Energy(string folder, int num, double T, double V){
	string file_name_str = folder + "/experiment_" + to_string(num) + ".csv";
	FILE *file;
	const char *file_name = file_name_str.c_str();
	file = fopen(file_name, "a");
	if (file == NULL) throw(-4);
	fprintf(file,"%e \t %e \t %e\n", T,V,T+V); 
	fclose(file);
}


/* Вывод всей системы */
void Out_All(string folder, int num, double x[GREAT_DIM]){
	FILE *file;

	// файл с номером num и расширением .vtk
	string file_name_str = folder + "/file_" + to_string(num) + ".csv";
	const char *file_name = file_name_str.c_str();
	int counter = 0;

	file = fopen(file_name, "a");
	if (file == NULL) throw(-4);

	for (int i=0; i < GREAT_DIM; i++){
		fprintf(file, "%e\t", x[i]);
	}
	fprintf(file,"\n");
	fclose(file);
}


void Out_All(string folder, int num, double x[DIM], double y[DIM], double z[DIM],double u[DIM], double v[DIM], double w[DIM]){
	FILE *file;

	// файл с номером num и расширением .vtk
	string file_name_str = folder + "/file_" + to_string(num) + ".csv";
	const char *file_name = file_name_str.c_str();
	int counter = 0;

	file = fopen(file_name, "a");
	if (file == NULL) throw(-4);


	for (int i=0; i < DIM; i++){
		fprintf(file, "%e \t %e \t %e \t %e \t %e \t %e \t",x[i],u[i],y[i],v[i],z[i],w[i]);
	}
	fprintf(file,"\n");
	fclose(file);
}


/* Вывод vtk, объявлен в system.h */
void Out_vtk(string folder, int num, double x[GREAT_DIM]){
	FILE *file;

	// файл с номером num и расширением .vtk
	string file_name_str = folder + "/file_" + to_string(num) + ".vtk";
	const char *file_name = file_name_str.c_str();
	int counter = 0;

	file = fopen(file_name, "w");
	if (file == NULL) throw(-4);

	fprintf(file, "# vtk DataFile Version 2.0\nAtoms example\nASCII\nDATASET POLYDATA\nPOINTS 4 float\n"); 

	for (int i=0; i < GREAT_DIM; i+=2){
		if (counter == 3){
			counter = 0;
			fprintf(file,"\n");
		}
		fprintf(file, "%e ", x[i]);
		counter++;
	}
	fprintf(file,"\n");
	fprintf(file, "POINT_DATA 4\nSCALARS MoleculeType float 1\nLOOKUP_TABLE default\n1\n1\n1\n1");
	fclose(file);
}

void Out_vtk(string folder, int num, double x[DIM], double y[DIM], double z[DIM]){
	FILE *file;

	// файл с номером num и расширением .vtk
	string file_name_str = folder + "/file_" + to_string(num) + ".vtk";
	const char *file_name = file_name_str.c_str();
	int counter = 0;

	file = fopen(file_name, "w");
	if (file == NULL) throw(-4);

	fprintf(file, "# vtk DataFile Version 2.0\nAtoms example\nASCII\nDATASET POLYDATA\nPOINTS 4 float\n"); 

	for (int i=0; i < DIM; i++){
		fprintf(file, "%e %e %e\n",x[i],y[i],z[i] );
	}
	fprintf(file,"\n");
	fprintf(file, "POINT_DATA 4\nSCALARS MoleculeType float 1\nLOOKUP_TABLE default\n1\n1\n1\n1");
	fclose(file);
}

