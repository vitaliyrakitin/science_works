#include "headers/system.h"

/* квадрат длины расстояния между двумя точками, определена system.h */
double Distance2(double x, double y, double z, double x1, double y1, double z1){
	double x2 = (x - x1) * (x - x1);
	double y2 = (y - y1) * (y - y1);
	double z2 = (z - z1) * (z - z1);
	return x2 + y2 + z2;
}
/* проверка расстояния между молекулами для выхода */
int check_dist(double koord[GREAT_DIM], double *result){
	double x[DIM];
	double y[DIM];
	double z[DIM];
	double dist = (dist_0 + 4) * (dist_0 + 4);
	
	for (int i=0; i< GREAT_DIM; i+= 6){
		 x[i/6] = koord[i + 0];
		 y[i/6] = koord[i + 2];
		 z[i/6] = koord[i + 4];
	}

	for (int i = 0; i < DIM-1; i++)
		for (int j = i+1; j< DIM; j++){
			*result = Distance2(x[i],y[i],z[i],x[j],y[j],z[j]);
			if (*result > dist)
				return 1;
		}

	return 0;
}

int check_dist(double x[DIM], double y[DIM], double z[DIM], double *result){
	double dist = (dist_0 + 1) * (dist_0 + 1);


	for (int i = 0; i < DIM-1; i++)
		for (int j = i+1; j< DIM; j++){
			*result = Distance2(x[i],y[i],z[i],x[j],y[j],z[j]);
			if (*result > dist)
				return 1;
		}

	return 0;
}

void MiddleVelocity(double *u,double *v,double *w,double *uc,double *vc,double *wc)
{	for (int i=0;i<molecul;i++)
	{
		uc[i] = (u[i*2] + u[i*2+1])/2;
		vc[i] = (v[i*2] + v[i*2+1])/2;
		wc[i] = (w[i*2] + w[i*2+1])/2;
	}
}
